# 気象庁HPのAMeDASや気象官署の観測値を取得するスクリプト
[過去の気象データ検索](http://www.data.jma.go.jp/obd/stats/etrn/index.php)より取得します．


## ダウンロード
お好きなディレクトリで以下のコマンドを実行して下さい
```
$ git clone https://gitlab.com/naos1/addamedas.git
```
するとaddamedasディレクトリが生成し，以下のファイル

* main.py: 本体のスクレイピングスクリプト
* select-station.py: データを取得する観測所を指定するスクリプト
* names.py: 変数名等を格納しているファイル(from names import * で利用)
* check-csv.ipynb: データを簡易的にチェックするjupyter notebook

と，以下のディレクトリ

* station: 観測地点の情報を取得するスクリプト群
* draw: 作図のためのPythonスクリプト．

がダウンロードされます．

## Python外部パッケージの要求

* numpy
* pandas
* scipy
* matplotlib
* cartopy
* netCDF4

Anaconda or Minicondaを使っているなら，`conda list`で確認し，もしなければ，`conda install [package]`，cartopyとnetCDF4に関しては`conda install -c conda-forge [package]`でインストールしてください．

<br>

# 利用法

1. select-station.pyによりstationデータの生成 (例, ngt.csv)
1. stationデータを指定してmain.pyの編集・実行
1. dataディレクトリに指定期間でまとめた観測所ごとのデータを保存

## select-station.py

select-station.pyを編集し，データを取得したい観測所を抽出します．抽出にはPandasのDataFrameのqueryメソッドを利用します．select-station.py内部にあらかじめ利用例をいくつか載せてあるので参考にしてください．

## main.py

* データのタイムステップを指定（10min, hourly, daily, monthlyのみ）
* 観測所を指定（select-station.pyで生成したstationファイルを指定）
* データ期間を指定
* 出力ディレクトリを指定

## 描画スクリプト

### timeseq.py
<img src=".img/timeseq-sample.png" width="500px">
<br>

地点と変数を指定して単純な時系列の折線グラフを作成するスクリプト，確認用   
実質，以下のワンコマンドの作図なので，お好きに改造してください．

```
   ax.plot(df.index.to_pydatetime(), df[val],
           color="red", linewidth=2.0, label=val)
```

### timeseq-snow.py
<img src=".img/timeseq-snow-sample.png" width="500px">
<br>

地点を指定して積雪，降雪，気温，気圧（あれば）の時系列グラフを作成するスクリプト．降雪なしだとバグります．


### plot-map.py
<img src=".img/plot-map-sample.png" width="500px">
<br>
<img src=".img/plot-map-wind-sample.png" width="500px">
<br>

地図上に観測ごとの値をプロットするスクリプト．風ベクトルも実装（`val = '風速'`かつ`direct = '風向'`と指定）   
2次元線形内挿をして空間分布を取得することもできます．   
県境は[GADM](https://gadm.org/download_country_v3.html)にて提供されている県境等のシェープファイル(gadm36_JPN_shp)を利用し，地形は[SRTM15+](https://topex.ucsd.edu/WWW_html/srtm15_plus.html)を東西南北にそれぞれ4グリッド間引いたデータ(SRTM15+jpn-skip5x5.nc)を利用しています．

* x: 欠損，観測なし(///)
* -: 観測なし(気象庁HPの表にない)
* .: 静穏



