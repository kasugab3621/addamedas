#!/usr/bin/env python3

# plot amedas map
# ks 2018.07.20
# 2021.02.18 kasuga

import os
import time
from datetime import datetime

import numpy as np
import pandas as pd

from scipy import interpolate 

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from pandas.plotting import register_matplotlib_converters

import mpl_toolkits.axes_grid1

import cartopy.crs as ccrs
import cartopy.feature as cftr
import cartopy.io.shapereader as shapereader

import netCDF4

from names import *

register_matplotlib_converters()  # pandasとmatplotlibの時刻表示の競合回避
matplotlib.use('Agg')  # Xwindowを介さずに作図

# 日本語フォントの選択
#matplotlib.rcParams['font.family'] = 'VL Gothic'  # centos
#matplotlib.rcParams['font.family'] = 'Noto Sans CJK JP'  # ubuntu
matplotlib.rcParams['font.family'] = 'Hiragino sans'  # macos


##### データのタイムステップを指定 #####
# 10min, hourly, daily, monthly から選択
tstep = 'daily'


##### 観測所ファイルのパスを指定 #####
sta = pd.read_csv('../ngt.csv', names=stn)


##### 描画する変数の指定 #####
val = '最深積雪'
direct = ''  # valに風速の時に風向を指定，それ以外は''
#print(names[tstep]['a1'])  # 確認用
#print(names[tstep]['s1'])
#quit()

# プロットの色合いの調整
vmin = 0  # minimum value
vmax = 200  # maximum value
#vint = 20  # 色の幅の間隔
cmap = 'rainbow'


##### 入出力ファイルのディレクトリを指定 #####
ddir = '../data'  # 入力
fdir = 'fig'  # 出力
os.makedirs(fdir ,exist_ok=True)


##### 描画範囲の指定 ######
#slat , nlat= 30., 46.
#elon , wlon= 147., 128.
slat , nlat= 36.25, 38.9
wlon , elon= 137.4, 140.


##### 描画する時間の範囲の自動指定（dummyを利用） #####
block = sta.iloc[0]['b']
town = sta.iloc[0]['地点名']
dummy = pd.read_csv(f'{ddir}/{tstep}-{block:04}-{town}.csv',
                    index_col='時刻', parse_dates=True,)

sdate = dummy.index[0]
edate = dummy.index[-1]

##### 描画する時間の範囲の任意指定 #####
#sdate = '2018-01-11 01:00:00'
#edate = '2018-01-12 00:00:00'

### 時間を管理する変数time ###
time = pd.date_range(start=sdate, end=edate, freq=freq[tstep])
it=len(time)


##### 県境データの読み込み #####
shape1 = list(shapereader.Reader('gadm36_JPN_shp/gadm36_JPN_1.shp').geometries())
shape0 = list(shapereader.Reader('gadm36_JPN_shp/gadm36_JPN_0.shp').geometries())


##### 地形データの読み込み #####
nc = netCDF4.Dataset('srtm15+/SRTM15+jpn-skip5x5.nc', 'r')
lons = nc.variables['longitude'][:]
lats = nc.variables['latitude'][:]
zs = nc.variables['z'][:,:]


##### 時刻のループ #####
for t in range(it) :
#for t in range(st, et+1) :


    ts = time[t]
    print(ts, val)
    
    yy = ts.year; mm = ts.month
    dd = ts.day; hh = ts.hour; mi = ts.minute

    tn = f'{yy}/{mm:02}/{dd:02} {hh:02}:{mi:02}'
    tnn = f'{yy}{mm:02}{dd:02}{hh:02}{mi:02}'

    
    # start making figure
    #fig = plt.figure(figsize=(8, 8))
    fig = plt.figure()
    
    # data projection
    dL = ccrs.PlateCarree()
    
    ## map projection    
    #cL = ccrs.Stereographic(
    #     central_longitude=clon,
    #     central_latitude=clat)
    
    ax = fig.add_subplot(1, 1, 1, projection=dL)
    
    # area extention
    ext=[wlon, elon, slat, nlat]
    ax.set_extent(ext, ccrs.PlateCarree())

    # 緯度経度線
    gl = ax.gridlines(xlocs=range(-180, 181, 1),
                      ylocs=range(-90, 91, 1),
                      draw_labels=True)
    gl.top_labels = False
    gl.right_labels = False

    # 国境
    ax.add_geometries(shape0, dL, edgecolor='black', facecolor='none', linewidths=0.5)
    
    # 県境
    ax.add_geometries(shape1, dL, edgecolor='black', facecolor='none', linewidths=0.5)


    # 地形
    levs = np.arange(500. ,3800., 500.)
    alt = ax.contourf(lons, lats, zs, levs, cmap='Greys', extend='both',
                      transform=dL, zorder=1)
    ax.contour(lons, lats, zs, levs, colors='k', transform=dL, 
               linewidths=0.3, zorder=1)
    #alt.cmap.set_under('w')
    

    ##### 観測所のループ #####
    flag = True
    for _s in range(len(sta)):


        ##### 個々の観測所情報の取得 #####
        s = sta.iloc[_s]
        type = s['type']  # a1: AMeDAS, s1: 気象官署
        block = s['b']  # 識別番号, [01]xxx: AMeDAS, 47xxx: 気象官署
        pb = s['pb']  # proc_no & blic_no
        town = s['地点名']  # カタカナ
        pref = s['県名']
        lon, lat = s['経度'], s['緯度']
        lev = s['標高']


        ##### データの読み込み #####
        df = pd.read_csv(f'{ddir}/{tstep}-{block:04}-{town}.csv',
                         index_col='時刻', parse_dates=True,)

        
        try:  ### データあり ###
            var1 = df[val].iloc[t]

            ##### 欠損値の処理 #####
            undef = -999.9
            if np.isclose(var1, undef):
                ax.plot(lon, lat, marker='x', markersize=5, color='k')
                continue

            ### 風向の取得 ###
            if direct != '':
                var2 = df[direct].iloc[t]

                ##### 静穏の処理 #####
                seion = -888.8
                if np.isclose(var2, seion):
                    ax.plot(lon, lat, marker='.', markersize=5, color='k')
                    continue

        except:  ### データなし ###
            ax.plot(lon, lat, marker='_', markersize=5, color='k')
            continue


        if flag:
            _lon, _lat = lon, lat
            _v1 = var1
            flag = False
            
            if direct != '':
                _cos = np.cos(np.deg2rad(var2))
                _sin = np.sin(np.deg2rad(var2))

        else:
            _lon = np.append(_lon, lon)
            _lat = np.append(_lat, lat)
            _v1 = np.append(_v1, var1)

            if direct != '':
                _cos = np.append(_cos, np.cos(np.deg2rad(var2)))
                _sin = np.append(_sin, np.sin(np.deg2rad(var2)))

    # remove undef
    #_v1 = np.where(_v1==-777, np.nan, _v1)


    if direct != '':        
        draw1 = ax.quiver(_lon, _lat, _cos, _sin, _v1, angles='xy',
                          clim=(vmin, vmax), cmap=cmap,
                          edgecolors='k', transform=dL, zorder=10)
        
    else:
        draw1 = ax.scatter(_lon, _lat, c=_v1, vmin=vmin, vmax=vmax, cmap=cmap,
                           s=40, edgecolors='k', transform=dL, zorder=10)
        
    
    '''空間内挿
    X, Y = np.meshgrid(lons, lats)

    X2 = np.linspace(np.amax(_lon), np.amin(_lon), len(_lon))
    Y2 = np.linspace(np.amax(_lat), np.amin(_lat), len(_lat))
    XX, YY = np.meshgrid(X2, Y2)

    v2 = interpolate.griddata((_lon, _lat), _v1, (XX, YY))
    cols = np.arange(vmin, vmax, vint)
    ax.contourf(XX, YY, v2, cols, transform=dL, cmap='rainbow', zorder=3)
    '''

    ##### タイトル（カラーバーより前に書く） #####
    plt.title(f'AMeDAS {tstep} {val} {ts}')


    ##### カラーバー #####
    divider = mpl_toolkits.axes_grid1.make_axes_locatable(ax)
    cax = divider.append_axes('right', '4%', pad='3%', axes_class=plt.Axes)
    cbar=fig.colorbar(draw1, cax=cax)
    cax.tick_params(labelsize=12)


    ##### 画像の保存 #####
    #plt.savefig(f'{tnn}.png', dpi=500)'
    plt.savefig(f'{fdir}/plotmap-{tstep}-{val}-{tnn}.png')
    plt.close()
