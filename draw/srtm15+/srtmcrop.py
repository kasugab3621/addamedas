import netCDF4
import numpy as np

N = 45.6
S = 20.4
E = 154.0
W = 122.9

nc = netCDF4.Dataset('SRTM15+V2.1.nc', 'r')
lons = nc.variables['lon'][72695:80161:5]
lats = nc.variables['lat'][26495:32545:5]
zs = nc.variables['z'][26495:32545:5,72695:80161:5]

#print(np.abs(np.asarray(lons) - E).argmin()) 80160
#print(np.abs(np.asarray(lons) - W).argmin()) 72695
#print(np.abs(np.asarray(lats) - S).argmin()) 26495
#print(np.abs(np.asarray(lats) - N).argmin()) 32544

onc = netCDF4.Dataset(f'SRTM15+jpn-skip5x5.nc', 'w', format='NETCDF4_CLASSIC')
onc.createDimension('lon', len(lons))
onc.createDimension('lat', len(lats))

longitude = onc.createVariable('longitude', np.float32, ('lon'))
#lon.long_name = 'east longitude'
#lon.units = 'degree of east longitude'

latitude = onc.createVariable('latitude', np.float32, ('lat'))
#lat.long_name = 'north latitude'
#lat.units = 'degree of north latitude'

z = onc.createVariable('z', np.float32, ('lat', 'lon'))
z.long_name = 'z'
z.units = 'meter'

longitude[:] = lons
latitude[:] = lats
z[:,:] = zs

onc.close()

quit()
