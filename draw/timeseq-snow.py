#!/usr/bin/env python3
# AMeDAS time-series
# アメダスデータを時系列表示
# 2018.08.28 ks

import os
from datetime import datetime
import numpy as np 
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()  # pandasとmatplotlibの時刻表示の競合回避
matplotlib.use('Agg')  # Xwindowを介さずに作図

# 日本語フォントの選択
#matplotlib.rcParams['font.family'] = 'VL Gothic'  # centos
#matplotlib.rcParams['font.family'] = 'Noto Sans CJK JP'  # ubuntu
matplotlib.rcParams['font.family'] = 'Hiragino sans'  # macos

from names import *


##### 観測所ファイルのパスを指定 #####
sta = pd.read_csv('../ngt.csv', names=stn)

### 個別指定 ###
select = ['ユザワ']
#select = sta['地点名'].values.tolist()  # 全地点指定


##### 入出力ファイルのディレクトリを指定 #####
ddir = '../data'  # 入力
fdir = 'fig'  # 出力
os.makedirs(fdir ,exist_ok=True)


##### データのタイムステップを指定 #####
# hourly固定
tstep = 'hourly'


##### 観測所のループ #####
for _s in range(len(sta)):

    
    ##### 個々の観測所情報の取得 #####
    s = sta.iloc[_s]
    type = s['type']  # a1: AMeDAS, s1: 気象官署
    block = s['b']  # 識別番号, [01]xxx: AMeDAS, 47xxx: 気象官署
    pb = s['pb']  # proc_no & blic_no
    town = s['地点名']  # カタカナ
    pref = s['県名']


    ##### 指定地点の作図 #####
    if town in select:

        print(town)

        
        ##### データの読み込み #####
        df = pd.read_csv(f'{ddir}/{tstep}-{block:04}-{town}.csv',
                         index_col='時刻', parse_dates=True,)


        ##### 欠損値の処理 #####
        undef = -999.9
        df.where(df != -999.9, np.nan, inplace=True)


        ##### 時刻の設定 #####
        sdate = df.index[0]
        edate = df.index[-1]

        
        ##### 描画期間の切り取り #####
        #df = df['2020-12-16 00':'2020-12-16 23']

        # x軸の目盛間隔
        #tlabel = pd.date_range(df.index[0], df.index[-1], freq='3h')
        # x軸の日ごとの縦線
        #dlabel = pd.date_range(df.index[0], df.index[-1], freq='d')        

        
        ##### Figureの作成 #####        
        # 作図開始
        fig = plt.figure(figsize=(9, 3))

        
        # axes（図の小区分）の設定
        # axes([left, bottom, width, height])
        # ax1:降雪, ax2:積雪, ax3:気温, ax4:海面気圧
        
        ax1 = fig.add_axes((0.1, 0.5, 0.8, 0.4))
        ax3 = fig.add_axes((0.1, 0.1, 0.8, 0.4), sharex=ax1)
        
        # ax1とax2,ax3とax4を関連させる
        ax2 = ax1.twinx()
        if type == 's1':
            ax4 = ax3.twinx()

        # （メインの作図）
        ax1.bar(df.index.to_pydatetime(), df['降雪'], color="blue", width=0.05,
                edgecolor="black", linewidth=0.2, label="fall")
        ax2.plot(df.index.to_pydatetime(), df['積雪'],
                 color="red", linewidth=2.0,label="depth")
        ax3.plot(df.index.to_pydatetime(), df['気温'], color="green",
                 linewidth=2.0, label="tmp")
        if type == 's1':
            ax4.plot(df.index.to_pydatetime(), df['海面気圧'], color="purple",
                     linewidth=2.0, label="slp")

        # 気温の正負で色付け（0付近でたまに変になるけど無視）
        ax3.fill_between(df.index.to_pydatetime(), df['気温'], 0.,
                         where=df['気温']>=0., facecolor='pink')
        ax3.fill_between(df.index.to_pydatetime(), df['気温'], 0.,
                         where=df['気温']<=0., facecolor='skyblue')


        ##### 図の調整 #####
        
        # （不要なx軸のラベルの削除）
        ax1.tick_params(labelbottom="off")
        ax2.tick_params(labelbottom="off")
        if type == 's1':
            ax4.tick_params(labelbottom="off")
        
        # y軸のラベルタイトル
        ax1.set_ylabel('降雪量 [cm/h]')
        ax2.set_ylabel('積雪量 [cm]')
        ax3.set_ylabel('気温 [Cdeg]')
        if type == 's1':
            ax4.set_ylabel('海面気圧 [hPa]')
        
        # ラベルの色と大きさ等の設定
        ax1.tick_params(axis='y', colors='blue', labelsize=12)
        ax2.tick_params(axis='y', colors='red', labelsize=12)
        ax3.tick_params(axis='y', colors='green', labelsize=12)
        if type == 's1':
            ax4.tick_params(axis='y', colors='purple', labelsize=12)
        
        # gridの設定（左のyラベルに揃えるようにしている）
        ax1.grid(True)
        ax2.grid(False)
        ax3.grid(True)
        if type == 's1':
            ax4.grid(False)
        

        ##### 図の細かい調整 #####
        
        # y軸の範囲 (plt.ylim)
        #ax1.set_ylim(0,10) #降雪
        #ax2.set_ylim(0,150) #積雪
        #ax3.set_ylim(-10,5.5) #気温
        #if type == 's1':
            #ax4.set_ylim(1013,1024) #海面気圧
        
        # y軸の目盛間隔（広めに設定するとよい）(plt.yticks)
        #ax1.set_yticks( np.arange(0, 15.1, 3) ) #降雪
        #ax2.set_yticks( np.arange(0, 50.1, 10) ) #積雪
        #ax3.set_yticks( np.arange(-5, 5.1, 2) ) #気温
        #if type == 's1':
        #ax4.set_yticks( np.arange(1010, 1020.1, 2) ) #海面気圧
        
        # y軸の範囲(山岳用) -------------------
        #ax1.set_ylim(0,15)
        #ax2.set_ylim(50,250)
        #ax3.set_ylim(-10,5)
        
        # y軸の目盛間隔(山岳用)
        #ax1.set_yticks( np.arange(0, 15, 3) )
        #ax2.set_yticks( np.arange(50, 300, 50) )
        #ax3.set_yticks( np.arange(-10, 5, 2.5) )
        #if type == 's1':
        #ax4.set_yticks( np.arange(1014, 1023, 4) )
        #  ------------------------------------

        # x軸の目盛間隔 (plt.xticks)
        #ax1.set_xticks(tlabel)
        #ax2.set_xticks(tlabel)
        #ax3.set_xticks(tlabel)
        #if type == 's1':
        #    ax4.set_xticks(tlabel)

        # x軸目盛の表示設定
        #ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H'))
        #ax3.xaxis.set_major_formatter(mdates.DateFormatter('%H'))
        
        # x軸の日付の縦線
        #ax1.vlines(dlabel, 0, 10, colors='k', linewidth=2.0, label='05')
        #ax3.vlines(dlabel, -6.5, 7, colors='k', linewidth=2.0, label='05')

        
        # たまにいい感じにしてくれるらしい？（任意）
        #plt.tight_layout()
        
        # 全体のタイトル
        ax1.set_title(f'{pref} {town} {sdate} - {edate}')
        
        # 画像の保存
        plt.savefig(f'{fdir}/timeseq-{tstep}-SNOW-{block:04}-{town}.png')
        
        #plt.show()
        plt.close()
