#!/usr/bin/env python3
# AMeDAS time-series
# アメダスデータを時系列表示
# 2018.08.28 ks
# 2021.02.17 ks

import os
import time
from datetime import datetime
import numpy as np 
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()  # pandasとmatplotlibの時刻表示の競合回避
matplotlib.use('Agg')  # Xwindowを介さずに作図

# 日本語フォントの指定
#matplotlib.rcParams['font.family'] = 'VL Gothic'  # centos
#matplotlib.rcParams['font.family'] = 'Noto Sans CJK JP'  # ubuntu
matplotlib.rcParams['font.family'] = 'Hiragino sans'  # macos

from names import *  # stn,namesを利用


##### データのタイムステップを指定 #####
# 10min, hourly, daily, monthly から選択
tstep = 'daily'


##### 観測所ファイルのパスを指定 #####
sta = pd.read_csv('../ngt.csv', names=stn)

### 個別指定 ###
select = ['ニイガタ']
#select = sta['地点名'].values.tolist()  # 全地点指定


##### 描画する変数の指定 #####
val = '平均気温'
#print(names[tstep]['a1'])  # 確認用
#print(names[tstep]['s1'])
#quit()

##### 入出力ファイルのディレクトリを指定 #####
ddir = '../data'  # 入力
fdir = 'fig'  # 出力
os.makedirs(fdir ,exist_ok=True)


##### 観測所のループ #####
for _s in range(len(sta)):

    
    ##### 個々の観測所情報の取得 #####
    s = sta.iloc[_s]
    type = s['type']  # a1: AMeDAS, s1: 気象官署
    block = s['b']  # 識別番号, [01]xxx: AMeDAS, 47xxx: 気象官署
    pb = s['pb']  # proc_no & blic_no
    town = s['地点名']  # カタカナ
    pref = s['県名']


    ##### 指定地点の作図 #####
    if town in select:

        print(town, val)

        
        ##### データの読み込み #####
        df = pd.read_csv(f'{ddir}/{tstep}-{block:04}-{town}.csv',
                         index_col='時刻', parse_dates=True,)


        ##### 欠損値の処理 #####
        undef = -999.9
        df.where(df != -999.9, np.nan, inplace=True)


        ##### 時刻の設定 #####
        sdate = df.index[0]
        edate = df.index[-1]


        ##### 描画期間の切り取り #####
        #df = df['2020-12-16 00':'2020-12-16 23']

        # x軸の目盛間隔
        #tlabel = pd.date_range(df.index[0], df.index[-1], freq='3h')


        ##### Figureの作成 #####
        #fig = plt.figure(figsize=(9, 3))
        fig = plt.figure()
        ax = fig.add_subplot(111)
        
        # 時刻の表示方法の設定 (\nは改行)
        ax.xaxis.set_major_formatter(
            mdates.DateFormatter('%m/%d\n%H:%M'))


        ##### メインの描画 #####
        # ax.plotをax.barに変えたり，色々できます
        # 詳しくはmatplotlibで検索してください
        ax.plot(df.index.to_pydatetime(), df[val],
                color="red", linewidth=2.0, label=val)

        ax.grid()

        # 図のタイトル
        ax.set_title(f'{pref} {town} {val} {sdate} - {edate}')
        


        ##### 図の細かい調整 #####

        # y軸の範囲 (plt.ylim)
        #ax.set_ylim(0,10) #降雪

        # y軸の目盛間隔（広めに設定するとよい）(plt.yticks)
        #ax.set_yticks( np.arange(0, 15.1, 3) ) #降雪

        # x軸（時間軸）の範囲 (plt.xlim)
        #ax.set_xlim(sdate, edate)

        # x軸の目盛間隔 (plt.xticks)
        #ax.set_xticks(tlabel)

        # x軸目盛の表示設定
        #ax.xaxis.set_major_formatter(mdates.DateFormatter('%H'))

        # y軸のラベルタイトル
        #ax.set_ylabel(val)

        # ラベルの色と大きさ等の設定
        #ax.tick_params(axis='y', colors='blue', labelsize=12)


        # 画像の保存
        plt.savefig(f'{fdir}/timeseq-{tstep}-{val}-{block:04}-{town}.png')

        #plt.show()
        plt.close()
