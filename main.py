#!/usr/bin/env python3
"""
気象庁HPのAMeDASや気象官署の観測値を取得するスクリプト

備考
・names.pyを同じ階層に置くこと
・準正常値は正常値として利用
・その他欠損値は-999，風向の静穏は-888


元スクリプト
https://met-learner.hatenablog.jp/entry/2019/12/15/123637

2020.02.11 kasuga
"""

##### ライブラリの読み込み #####
import os
import time
from datetime import datetime
import numpy as np
import pandas as pd
from names import *  # names,stn,freq,pfreqを利用


##### データのタイムステップを指定 #####
# 10min, hourly, daily, monthly から選択
tstep = 'hourly'


##### 観測所を指定 #####
sta = pd.read_csv('ngt.csv', sep=',', names=stn)


##### データ期間を指定 #####
tt = pd.date_range(start='2020/12/15 00:00',
                     end='2020/12/17 23:00',
                     freq=pfreq[tstep])


##### 出力ディレクトリを指定 #####
fdir = 'data'
os.makedirs(fdir ,exist_ok=True)


#----- 以下は修正不要です -----#


##### 観測所のループ #####
for _s in range(len(sta)):

    mode = 'w'
    header = True

    
    ##### 個々の観測所情報の取得 #####
    s = sta.iloc[_s]
    type = s['type']  # a1: AMeDAS, s1: 気象官署
    block = s['b']  # 識別番号, [01]xxx: AMeDAS, 47xxx: 気象官署
    pb = s['pb']  # proc_no & blic_no
    town = s['地点名']  # カタカナ
    pref = s['県名']
    

    ##### データページのループ #####
    for t in tt:
        
        year = t.year
        month = t.month
        day = t.day


        ##### 進捗状況の出力 #####
        if tstep == '10min' or tstep == 'hourly':
            print('downloading ... ', tstep , f'{year}/{month:02}/{day:02}', '@', pref, town)
        if tstep == 'daily':
            print('downloading ... ', tstep , f'{year}/{month:02}', '@', pref, town)
        if tstep == 'monthly':
            print('downloading ... ', tstep , f'{year}', '@', pref, town)


        ##### urlを指定#####
        u1 = 'http://www.data.jma.go.jp/obd/stats/etrn/view/'
        u2 = '&view=p1'
        url = f'{u1}{tstep}_{type}.php?{pb}&year={year}&month={month}&day={day}{u2}'


        ##### スクレイピング #####
        try:
            tables = pd.io.html.read_html(url)
            #df = tables[0].iloc[rmrows[tstep][type]:, 1:]
            df = tables[0].iloc[:, 1:]
            df = df.reset_index(drop = True)
        except ValueError:
            print('ERROR: 指定したデータは存在しません')
            exit(1)
            

        ##### 列名を指定 #####
        df.columns = names[tstep][type]


        ##### 欠測値の処理 #####
        undef = '-999.9'
        df = df.replace('///', undef)  # --- '///' を欠測値として処理
        df = df.replace('×', undef)  # --- 'x' を欠損値として処理
        df = df.replace('\s\)', '', regex = True)  # --- ')' が含まれる値を正常値として処理
        df = df.replace('.*\s\]', undef, regex = True) # --- ']' が含まれる値を欠測値として処理
        df = df.replace('#', undef)  # --- '#'が含まれる値を欠測値として処理
        df = df.replace('--', undef)  # --- '--'が含まれる値を欠測値として処理
        df = df.fillna(0)  # --- NaN を欠測値として処理


        ##### 風向を東0度で反時計回りの表記に変更 #####
        df = df.replace('東', '-180.0')
        df = df.replace('東北東', '-157.5')
        df = df.replace('北東', '-135.0')
        df = df.replace('北北東', '-112.5')
        df = df.replace('北', '-90.0')
        df = df.replace('北北西', '-67.5')
        df = df.replace('北西', '-45.0')
        df = df.replace('西北西', '-22.5')
        df = df.replace('西', '0.0')
        df = df.replace('西南西', '22.5')
        df = df.replace('南西', '45.0')
        df = df.replace('南南西', '67.5')
        df = df.replace('南', '90.0')
        df = df.replace('南南東', '112.5')
        df = df.replace('南東', '135.0')        
        df = df.replace('東南東', '157.5')
        df = df.replace('静穏', '-888.8')


        ##### 雲量10-を9.5に変換 #####
        df = df.replace('10-', 9.5)


        ##### ページ内の時刻列を作成 #####
        smin, sh, sd, sm = 0, 0, day, month
        
        # 10minデータは00:10から開始
        if tstep == '10min':
            smin = 10
        # hourlyデータは1時から開始
        if tstep == 'hourly':
            sh = 1
        # dailyデータは1日から開始
        if tstep == 'daily':
            sd = 1
        # monthlyデータは1月から開始
        if tstep == 'monthly':
            sm = month

        date = pd.date_range(datetime(year, sm, sd, sh, smin, 0),
                             periods=len(df), freq=freq[tstep])

        
        ##### 年/月/日/時/分/秒 の各列を追加 #####
        #df.insert(0, '年', date.year)
        #df.insert(1, '月', date.month)
        #df.insert(2, '日', date.day)
        #df.insert(3, '時', date.hour)
        #df.insert(4, '分', date.minute)

        ##### date (pd.date_range) を追加 #####
        df.insert(0, '時刻', date)
        
        ##### csvファイルとして出力 #####
        df.to_csv(f'{fdir}/{tstep}-{block:04}-{town}.csv',
                  header=header, index=False, mode=mode)

        
        if header:
            mode = 'a'
            header = False


        ##### 処理の停止時間を指定 #####
        time.sleep(0)
