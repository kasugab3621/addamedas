#!/usr/bin/env python3
"""
データを取得したい観測所を絞るスクリプト

station/all.master.20210212.csvを利用します

最新の情報に更新したい場合，
station/sta2csv.shを実行することで，
気象庁HPよりデータを抽出し
all.csvを再生成します．

文字化けする場合は，nkfをインストールし，
$ nkf all.csv > tmp
$ mv tmp all.csv
である程度は誤魔化せます

佐賀県，滋賀県，網走・北見・紋別
これらは手動で修正する必要があります．
（出来上がったものがall.master.20210212.csvです）

また，フジサンは山梨と静岡のものがありますが，
静岡の方を消しています.（深い意味はない）

"""
import os
import time
from datetime import datetime
import numpy as np
import pandas as pd


# 観測所情報のリスト
stn = ['type','b','pb','緯度','経度','地点名','県名', '標高','雨量計','風向風速計','湿度計','日射計','積雪深計','観測終了日']


# all.csvの読み込み
df = pd.read_csv('station/all.master.20210212.csv', sep=',', encoding='UTF-8', names=stn)


# 現役の観測所を選択
df = df.query('観測終了日 == 99999999')


# 気象官署(s1)のみorアメダス(a1)のみを選択
#df = df.query('type == "s1"')


# 新潟県内の観測所のみ選択
df = df.query('県名 == "新潟県"')


# 複数選択
#df = df.query('県名 == ["新潟県","群馬県"]')


# 積雪計のある観測所のみ選択
#df = df.query('積雪深計 == 1')


# 標高200m以上の観測所のみ選択
#df = df.query('標高 >= 200')


# csv出力
of='ngt.csv'
df.to_csv(of, header=False, index=False)

print(of)
print('該当数:', len(df))
