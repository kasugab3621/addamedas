#! /bin/bash

# get AMeDAS station meta-data 
# 2015.12.11 kasuga

# OSX sed -> gsed 
sed=sed

#set -exu

dname="all.csv"
rm -f $dname
touch $dname

while read pn ; do

# get source
wget --wait=3 "http://www.data.jma.go.jp/obd/stats/etrn/select/prefecture.php?prec_no=${pn}&block_no=&year=&month=&day=&view=" -O hoge
#cp hoge ../source/$pn.txt

# get pref name (using ZENKAKU-space, if any bags, check japanese codec)
ppname=`cat hoge | grep '<tr><td class="nwtop" colspan="2" style="font-weight:bold; text-align:left">' | tr -d '　'`
pname=`echo $ppname | $sed -e "s/<[^>]*>//g"`
echo $pname

# rough truncation
cl1=`cat hoge | grep -n '<map name="point">' | cut -d':' -f1`
$sed -i -e '1,'$cl1'd' hoge

cl2=`cat hoge | grep -n '</map>' | cut -d':' -f1`
$sed -i -e $cl2',$d' hoge

# cut other pref
cat hoge | grep -v 'prec_no=50'> fuga
cat fuga | grep -v 'prec_no='> hoge

# trim fild separation
$sed -i -e 's/onmouseover="javascript:viewPoint(//g' hoge
$sed -i -e 's/);" onmouseout="javascript:initPoint();">//g' hoge
$sed -i -e 's/'\''//g' hoge

# delete double record
wc=`cat hoge | wc -l`
for i in `seq 2 1 $wc` ; do
    ip=`expr $i - 1`
ipn=`awk 'BEGIN{FS=","}NR=='$ip'{print $2}' hoge` 
in=`awk 'BEGIN{FS=","}NR=='$i'{print $2}' hoge` 
if [ $ipn = $in ];then 
$sed -i -e ${i}d hoge
fi
done

# convert latlon sec -> deg & replace fields
echo $pname
awk 'BEGIN{FS=",";OFS=","}{print $1"1",$2,"prec_no="'$pn'"&block_no="$2,$5+$6/60,$7+$8/60,$4,"'$pname'",$9,$10,$11,$12,$13,$14,$15$16$17}' hoge >> $dname


done < prec_no.txt
rm -f hoge fuga

exit
